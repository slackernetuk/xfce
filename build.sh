# Xfce Build Script
#!/bin/bash

src=(
libdisplay-info 
gtk-layer-shell 
wlr-protocols 
libopenraw 
gnome-common 
bamf 
gimp-data-extras 
cogl 
clutter 
xfce4-dev-tools 
libxfce4util 
xfconf 
libxfce4ui 
libxfce4windowing 
exo 
garcon 
xfce4-panel 
thunar 
thunar-volman 
tumbler 
xfce4-appfinder 
xfce4-power-manager 
xfce4-settings 
xfdesktop 
xfwm4 
xfce4-session 
xfce4-pulseaudio-plugin 
xfce4-whiskermenu-plugin
xfdashboard-git 
xfce4-terminal 
xfce4-screensaver 
xfce4-screenshooter 
xfce4-notifyd 
xiccd 
cantarell-fonts 
Orchis-theme 
Tela-icon-theme 
)


for i in ${src[@]} ; do
cd ${i} || exit 1
sh ${i}.SlackBuild || exit 1
cd ..
done



